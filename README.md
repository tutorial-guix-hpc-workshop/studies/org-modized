# studies: Org-modized study

[![pipeline status](https://gitlab.inria.fr/tutorial-guix-hpc-workshop/studies/org-modized/badges/main/pipeline.svg)](https://gitlab.inria.fr/tutorial-guix-hpc-workshop/studies/org-modized/-/commits/main)
[![SWH](https://archive.softwareheritage.org/badge/swh:1:dir:2277b3d721d80119c2508aa919b5f73bb8f1e3d7/)](https://archive.softwareheritage.org/swh:1:dir:2277b3d721d80119c2508aa919b5f73bb8f1e3d7;origin=https://gitlab.inria.fr/tutorial-guix-hpc-workshop/studies/org-modized;visit=swh:1:snp:1edce0f7f7b974994cd04c50bf83c4de2f4f7167;anchor=swh:1:rev:23d07d1980cb487cb4036238ecb72328527a82ee)

This repository contains a copy of a minimal working example of an experimental
study written following the principles of literate programming [2]. It features
the `minisolver` application [3] and relies on the GNU Guix [1] transactional
package manager and to ensure reproducibility of the research study. We refer to
this version of the study as to the *Org-modized* study.

[Manuscript](https://tutorial-guix-hpc-workshop.gitlabpages.inria.fr/studies/org-modized/study.pdf).

## Reproducing guidelines

The entire study is self-contained within the [study.org](study.org) file. It is
written in Org mode, a literate programming tool. It combines the study itself
with all of the source code and guidelines required for reproducing it.

## References

1. GNU Guix software distribution and transactional package manager
   [https://guix.gnu.org](https://guix.gnu.org).
2. Literate Programming, Donald E. Knuth, 1984
   [https://doi.org/10.1093/comjnl/27.2.97](https://doi.org/10.1093/comjnl/27.2.97).
3. minisolver, a simple application for testing dense solvers with pseudo-BEM
   matrices
   [https://gitlab.inria.fr/tutorial-guix-hpc-workshop/software/minisolver](https://gitlab.inria.fr/tutorial-guix-hpc-workshop/software/minisolver).
